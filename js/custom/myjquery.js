

$('#c_name').on('keyup',function()
{
	//alert(name);
	val =$(this).val();
	if(val.match(/^[a-zA-Z]+$/))
	{
		$('#errorc_name').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errorc_name').show();
		$('#errorc_name').html('Enter valid  Name');
		$('#errorc_name').css('color','red');
	}
});	

$('#c_lname').on('keyup',function()
{
	//alert(name);
	val =$(this).val();
	if(val.match(/^[a-zA-Z]+$/))
	{
		$('#errorc_lname').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errorc_lname').show();
		$('#errorc_lname').html('Enter valid Last Name');
		$('#errorc_lname').css('color','red');
	}
});	



$('#c_email').on('change',function()
{
	val =$(this).val();
	if(val.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i))
	{
		$('#errorc_email').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errorc_email').show();
		$('#errorc_email').html('Enter valid Email');
		$('#errorc_email').css('color','red');
	}
});

