<?php 
include("config.php");
?>
<html>
<head>
  <title>Pride Creation | Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  
  <link rel="stylesheet" type="text/css" href="css/style.css">
  

  <link href="css/flexslider.css" rel="stylesheet">  
  <link href="css/animate.css" rel="stylesheet"> 

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script type="text/javascript">

    jQuery(document).ready(function($) {
      $(".scroll").click(function(event){   
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
      });
    });

  </script> 

</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

  <nav class="navbar navbar-default navbar-fixed-top"  style="background-color:#000000; margin:auto; height: 72px;">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <img src="images\logo.png"  height="70px"/>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="index.php"><font color="red">HOME</font></a></li>
          <li><a href="about.html"><font color="white">ABOUT US</font></a></li>
          <li><a href="services.html"><font color="white">SERVICES</font></a></li>
          <li><a href="gallery.php"><font color="white">GALLERY</font></a></li>
          <li><a href="contact.php"><font color="white">CONTACT</font></nav></a></li>
        </ul>
      </div>
    </div>
  </nav>



  <div class="w3-slider"> 
    <ul id="demo1">
      <?php 
             
             $sq_product= "SELECT * FROM slider";
             $result = mysqli_query($conn, $sq_product);
             while($row_slide = mysqli_fetch_assoc($result))
             {
             ?>
      <li>
        
         <img src="<?php echo 'uploads/'.$row_slide['slider_image']; ?>" alt="" />
        <div class="slide-desc">
          <h3><?php echo $row_slide['slider_name'] ?>..! </h3>
        </div>
      </li>
      <?php } ?>
     
      
    </ul>
  </div>

  <div>

    

   <section class="about" id="about" >
    <div class="container" style="margin:auto; margin-top: 10px;">
     <div class="row">
      

       <div class="col-md-6">
        <div class="head_title ">
          <h1 style="text-align: center;">WHAT WE DO ..?</h1></div>
          <blockquote class="text-justify" style="background-color: white !important; font-family: times new roman;">
            <p>
              At, Pride Creation, we have been weaving this magic through our Photo and Videography services to 1000 of customers who wanted to capture Candid Photographical moments in their life. Be it their Wedding, Engagement, Anniversary celebrations, Corporate events etc. We have captured their best moments that are imprinted in photos and videos forever.
            </p>
            <p>
              We provides a wide range of professional photography services such as Industrial, product, interior, design, Modeling, Wedding Photography, Album designing, posters, press photography, artistic productions, web sites, blogues and calendors, etc. Whether you are an Individual or a Corporate; No matter whether your requirement is big or small, Toehold armed with a team of in-house professionals and associates will deliver to your requirements with utmost care and quality. 
            </p>
            <p>Pride Creation, a video editing firm offers outsourcing options to entrepreneurs in both large and small companies, corporate needs such as training and product videos, short film makers, documentary producers, news agencies as well as individual videos like weddings, birthdays and holiday videos.</p>
            
          </blockquote>
        </div>

        <div class="col-md-6">
          <img src="images\5.jpg" style="margin-top: 50px; margin-bottom: 2em" width="100%">
        </div>
        
      </div>
    </div>
  </section>

  <!--client section reviw-->

  <div id="testimonials" class="testimonials">
    <div class="container">
      <div class="agileits_w3layouts_heding test-heading">
        <h3>What <span>Our Clients</span> Say</h3>
      </div>
      <div class="w3_agile_team_grids">
        
        <section class="slider">
          <div class="flexslider">

            <ul class="slides">
             <?php 
             
             $sq_product= "SELECT * FROM testimonial";
             $result = mysqli_query($conn, $sq_product);
             while($row = mysqli_fetch_assoc($result))
             {
             ?>
             <li>
              <div class="agile_testimonials_grid">
                <div class="agileits_w3layouts_testimonials_grid">
                  <img src="<?php echo 'uploads/' .$row['test_image'];?>" alt=" " class="img-responsive" />
                </div>
                <h3> <span><?php echo $row['test_name']; ?><br/><h5><?php echo $row['test_designation']; ?></h5></span></h3>
                <p><?php echo $row['test_desc']; ?>...</p>
              </div>
            </li>
           <!--    <li>
                <div class="agile_testimonials_grid">
                  <div class="agileits_w3layouts_testimonials_grid">
                    <img src="images/7.jpg" alt=" " class="img-responsive" />
                  </div>
                  <h3> <span>Chetan Gandhi<br/><h5>P.M.Shaha Foundation,Pune</h5></span></h3>
                  <p>We are very pleased with the result and love the service that they provided.....</p>
                </div>
              </li> -->
             <!--  <li>
                <div class="agile_testimonials_grid">
                  <div class="agileits_w3layouts_testimonials_grid">
                    <img src="images/8.jpg" alt=" " class="img-responsive" />
                  </div>
                  <h3> <span>Bhushan Godbole<br/><h5>Founder & director Of Aryaamoney</h5></span></h3>
                  <p>I have worked with Pride Creation on numerous projects, They are very co-operative,great job with photography,video shoot and editing needs..Thanks Once again...</p>
                </div>
              </li> -->
              <!-- <li>
                <div class="agile_testimonials_grid">
                  <div class="agileits_w3layouts_testimonials_grid">
                    <img src="images/9.jpg" alt=" " class="img-responsive" />
                  </div>
                  <h3> <span>Bhushan<br/><h5>Invent Production and Services Pvt. Ltd</h5></span></h3>
                  <p>We have received good service from Pride Creation, whenever I gave a call at eleventh hrs to Raviji, he said no problem Bhushanji, We will do it, It's best part I guess, designing and creativity is amazing..and he is providing stuff within short time span.... </p>
                </div>
              </li> -->
              <?php }?>
            </ul>
          </div>
        </section>
      </div>
    </div>
  </div>


  <div class="w3_agile_footer">
    <div class="container-fluid" style="background-color: #212121; text-align: center;">
      <p class="text-muted" style="margin-top: 15px;"><font size="3px">© 2017 Pride Creation . All rights reserved | Designed by </font><a href="http://sungare.com">Sungare.</a></p>
      <div class="arrow-container animated fadeInDown">
        <a href="#home" class="arrow-2 scroll scrollToTop">
          <i class="fa fa-angle-up"></i>
        </a>
        <div class="arrow-1 animated hinge infinite zoomIn"></div>
      </div>
    </div>
  </div>

</div>
<!--script-->
<!-- Required JavaScript Libraries -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>

<script src="js/skdslider.min.js"></script>
<link href="css/skdslider.css" rel="stylesheet">
<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
    
    jQuery('#responsive').change(function(){
      $('#responsive_wrapper').width(jQuery(this).val());
    });
    
  });
</script>


<script type="text/javascript" src="js/bootstrap.js"></script>



<script defer src="js/jquery.flexslider.js"></script>
<script type="text/javascript">
  $(window).load(function () {
    $('.flexslider').flexslider({
      animation: "slide",
      start: function (slider) {
        $('body').removeClass('loading');
      }
    });
  });
</script>

<script src="js/superfish.min.js"></script>
<script src="js/sticky.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript" src="js/wow.js"></script>
<script src="js/main.js"></script>


<script src="js/responsiveslides.min.js"></script>
<script src="js/jarallax.js"></script>
<script src="js/SmoothScroll.min.js"></script>
<script type="text/javascript">
  /* init Jarallax */
  $('.jarallax').jarallax({
    speed: 0.5,
    imgWidth: 1366,
    imgHeight: 768
  })
</script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!-- here stars scrolling icon -->
<script type="text/javascript">
  $(document).ready(function() {
      /*
        var defaults = {
        containerID: 'toTop', // fading element id
        containerHoverID: 'toTopHover', // fading element hover id
        scrollSpeed: 1200,
        easingType: 'linear' 
        };
        */
        
        $().UItoTop({ easingType: 'easeOutQuart' });
        
      });
    </script>
    <!-- //here ends scrolling icon -->
    <script type="text/javascript">
      $(window).load(function() {
        $("#flexiselDemo1").flexisel({
          visibleItems:3,
          animationSpeed: 1000,
          autoPlay: true,
          autoPlaySpeed: 3000,        
          pauseOnHover: true,
          enableResponsiveBreakpoints: true,
          responsiveBreakpoints: { 
            portrait: { 
              changePoint:480,
              visibleItems: 1
            }, 
            landscape: { 
              changePoint:640,
              visibleItems:2
            },
            tablet: { 
              changePoint:768,
              visibleItems: 3
            }
          }
        });
        
      });
    </script>
    <script type="text/javascript" src="js/jquery.flexisel.js"></script>



    <script src="js/skdslider.min.js"></script>
    <link href="css/skdslider.css" rel="stylesheet">
    <script type="text/javascript">
      jQuery(document).ready(function(){
        jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
        
        jQuery('#responsive').change(function(){
          $('#responsive_wrapper').width(jQuery(this).val());
        });
        
      });
    </script>


  </body>
  </html>