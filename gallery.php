﻿<?php 
include("config.php");
?>
<html>
<head>
	<title>Pride Creation | Gallery </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">

	<!--// bootstrap-css -->
	<!-- css -->
	<link rel="stylesheet" href="css/style2.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!--// css -->
	<link rel="stylesheet" href="css/font-awesome.min.css">

	<link rel="stylesheet" href="css/normalize2.css">
	<link rel="stylesheet" href="css/layout.css">

	<!-- //font-awesome icons -->
	<link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen">
	<!-- <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script> -->
	<!-- bootstrap-css -->
	<!--// css -->
	<!-- font-awesome icons -->
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

	<nav class="navbar navbar-default navbar-fixed-top"  style="background-color:#000000; margin:auto; height: 70px;">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>                        
				</button>
				<img src="images\logo.png"  height="70px"/>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="index.php"><font color="white">HOME</font></a></li>
					<li><a href="about.html"><font color="white">ABOUT US</font></a></li>
					<li><a href="services.html"><font color="white">SERVICES</font></a></li>
					<li class="active"><a href="gallery.php"><font color="red">GALLERY</font></a></li>
					<li><a href="contact.php"><font color="white">CONTACT</font></nav></a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div style="background-color: #e0e0e0;">

		<div class="container-fluid" style="background-position: center; background-repeat: no-repeat; margin:auto; height:250px; width: 100%; background-image: url('images/35.jpg'); filter: blur(0px);">
			<h1 style="text-align:center; margin-top: 150px;"><font size="60px" color="white" ;>GALLERY</h1></font>
		</div>
		<div class="gallery" id="projects">
			<div class="container">
				
				<ul id="filters" class="clearfix">				
				<li><span class="filter active" data-filter=".all">All</span></li>
					

					<?php 
					$res = $conn->query('select * from category');
					if($res->num_rows){
                         // $k = 0;
						while ($row = $res->fetch_assoc()) {

							echo '<li><span class="filter" data-filter=".'.strtolower(str_replace(' ', '-', $row['cat_name'])).'">'.$row['cat_name'].'</span></li>';

						}
					}
					?>  

				</ul>
				<?php 
				$res = $conn->query('select * from gallary inner join category on category.cat_id = gallary.category_id ');      
				if($res->num_rows){
                     		//$i = 0;
					?>
					<div id="portfoliolist">
						<div class="filtr-container filter" >
							<?php
							while ($row_gal = $res->fetch_assoc()) 
							{


								echo '<div class="portfolio filtr-item all '.strtolower(str_replace(' ', '-', $row_gal['cat_name'])).'"  style="height: 25%;" data-cat="'.strtolower(str_replace(' ', '-', $row_gal['cat_name'])).'">'
								.'<div class="gallery-t">'
								.'<a href="uploads/' .$row_gal['gal_image'].'"  class="b-link-stripe thickbox">'
								.'<figure>'
								.'<img src="uploads/'.$row_gal['gal_image'].'" class="img-responsive" alt=" " />	'
								.'<figcaption>'
								.'<h3>'.$row_gal['cat_name'].'</h3>'
								.'<p>'
								. $row_gal['gal_title']
								.'</p>'
								.'</figcaption>'
								.'</figure>'
								.'</a>'
								.'</div>'
								.'</div>';
							}
							?>


						</div> 
					</div> 
			<?php  } ?>

		</div>
	</div>
</div>
</div>
<div class="clearfix"> </div>

<section id="trial" class="trial text-center wow fadeIn"  data-wow-duration="2s" data-wow-dealy="1.5s">
	<div class="main_trial_area">
		<div class="container">
			<div class="row">
				<div class="main_trial">
					<div class="col-md-12">
						<h2><span>OUR GOAL</span> </h2>
						<p>Our goal is to give you the best experience and to let your emotions flow on the D-Day so that you can let go all your worries and enjoy the moment.. We focus on natural expressions so when you flip the chapters of your life, you realize what you were feeling or what you did at the same time.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>




<div class="w3_agile_footer">
	<div class="container-fluid" style="background-color: #212121; text-align: center;">
		<p class="text-center text-muted" style="margin-top: 15px;"><font size="3px">© 2017 Pride Creation . All rights reserved | Designed by </font><a href="http://sungare.com">Sungare.</a></p>
		<div class="arrow-container animated fadeInDown">
			<a href="#home" class="arrow-2 scroll scrollToTop">
				<i class="fa fa-angle-up"></i>
			</a>
			<div class="arrow-1 animated hinge infinite zoomIn"></div>
		</div>
	</div>
</div>

</div>

<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>

<script type="text/javascript">
	$(function () {

		var filterList = {
			
			init: function () {
				
					// MixItUp plugin
					// http://mixitup.io
					$('#portfoliolist').mixItUp({
						selectors: {
							target: '.portfolio',
							filter: '.filter'	
						},
						load: {
							filter: '.all'  
						}     
					});								

				}

			};
			
		// Run the show!
		filterList.init();
		
		
	});	
</script>
<script src="js/jquery.chocolat.js"></script>
<!--light-box-files -->
<script>
	$(function() {
		$('.filtr-item a').Chocolat();
	});
</script>

</body>
</html>