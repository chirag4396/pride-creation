
$('#frmtest').on('submit', function(e){
    e.preventDefault();

$('#success_message').fadeIn().html('Checking Data');
$("#btnup").prop('disabled', true);
var fd = new FormData(this);
$.ajax({
    url: 'update_test.php',
    type: 'POST',
    data:  fd,
    contentType: false,
    processData:false,
    success: function(data) 
    {
          //alert(data);
            $('#success_message').fadeIn().html('Data Updated sucessfully');
            setTimeout(function() {
                $('#frmtest')[0].reset();
                $("#btnup").prop('disabled', false);
                $('#success_message').fadeOut("slow");
            }, 1000 );
        },
    });
});