
$('#frmtest').on('submit', function(e){
    e.preventDefault();

$('#success_message').fadeIn().html('Checking Data');
$("#btnadd").prop('disabled', true);
var fd = new FormData(this);
$.ajax({
    url: 'add_testimonial.php',
    type: 'POST',
    data:  fd,
    contentType: false,
    processData:false,
    success: function(data) 
    {
          
            $('#success_message').fadeIn().html('Data Submitted sucessfully');
            setTimeout(function() {
                $('#frmtest')[0].reset();
                $("#btnadd").prop('disabled', false);
                $('#success_message').fadeOut("slow");
            }, 1000 );
        },
    });
});