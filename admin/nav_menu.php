<nav class="main-menu">
		<ul>
			<li>
				<a href="dashborad.php">
					<i class="fa fa-home nav_icon"></i>
					<span class="nav-text">
					Dashboard
					</span>
				</a>
			</li>
				<li class="has-subnav">
				<a href="javascript:;">
				<i class="fa fa-check-square-o nav_icon"></i>
				<span class="nav-text">
				Master
				</span>
				<i class="icon-angle-right"></i><i class="icon-angle-down"></i>
				</a>
				<ul>
				
					<li>
						<a class="subnav-text" href="testimonial.php">Add Testimonial</a>
					</li>
				<li>
						<a class="subnav-text" href="category.php">Add Category</a>
					</li>
				<li>
						<a class="subnav-text" href="gallery.php">Add Gallery</a>
					</li>
					<li>
						<a class="subnav-text" href="slider.php">Add Slider</a>
					</li>
				
				</ul>
			</li>
			<li>
				<a href="dashborad_testimonial.php">
					<i class="icon-table nav-icon"></i>
					<span class="nav-text">
					Dashborad Testimonial
					</span>
				</a>
			</li>

			<li>
				<a href="dashborad_gallery.php">
					<i class="icon-table nav-icon"></i>
					<span class="nav-text">
					Dashborad Gallary
					</span>
				</a>
			</li>
			<li>
				<a href="dashborad_category.php">
					<i class="icon-table nav-icon"></i>
					<span class="nav-text">
					 Dashborad Category
					</span>
				</a>
			</li>
			
			<li>
				<a href="dashborad_contact.php">
					<i class="icon-table nav-icon"></i>
					<span class="nav-text">
						Dashborad Contact
					</span>
				</a>
			</li>
			<li>
				<a href="dashborad_slider.php">
					<i class="icon-table nav-icon"></i>
					<span class="nav-text">
						Dashborad Slider
					</span>
				</a>
			</li>
			
					</ul> 
		<ul class="logout">
			<li>
			<a href="logout.php">
			<i class="icon-off nav-icon"></i>
			<span class="nav-text">
			Logout
			</span>
			</a>
			</li>
		</ul>
	</nav>