<html>
<head>
  <title>Contact to Pride Creation</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top"  style="background-color:#000000; margin:auto; height: 72px;">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <img src="images\logo.png" height="70px">
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
      <li><a href="index.php"><font color="white">HOME</font></a></li>
      <li><a href="about.html"><font color="white">ABOUT US</font></a></li>
      <li><a href="services.html"><font color="white">SERVICES</font></a></li>
      <li><a href="gallery.php"><font color="white">GALLERY</font></a></li>
      <li class="active"><a href="contact.php"><font color="red">CONTACT</font></nav></a></li>
      </ul>
    </div>
  </div>
</nav>



<div style="background-color: #e0e0e0;">

<div class="container-fluid" style="background-position: center; background-repeat: no-repeat; margin:auto; height:250px; width: 100%; background-image: url('images/35.jpg'); filter: blur(0px);">
<h1 style="text-align:center; margin-top:150px;"><font size="60px" color="white" ;>CONTACT  US</h1></font>
</div>

<div>
<div class="container-fluid" style=" height:70px; width: 100%;">

</div>
	
	
<div class="map">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d472.94039302722155!2d73.82388120674482!3d18.505246871383523!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xedc4ee799a991ef4!2sYatri+Hotel!5e0!3m2!1sen!2sin!4v1496468899206" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>		




<div class="contact-area">
	<div class="container">
	  <div class="row">
	    <!-- contact-info start -->
	    <div class="col-md-6 col-sm-12 col-xs-12">
	      <div class="contact-info">
	        <h3>Contact info</h3>
	        <ul>
	          <li>
	            <i class="fa fa-map-marker"></i> <strong>Address</strong>
	            S.No. 41/2A, Office No.5 , 
				2nd Floor, Ramchandra Apart., 
				Karve Road, Paud Phata, Near Yatri Hotel,
				Kothrud, Pune 38
	          </li>
	          <li>
	            <i class="fa fa-phone"></i> <strong>Phone</strong>
	            +91-7350950306 / +91-8805560629
	          </li>
	          
	          <li>
	            <i class="fa fa-envelope"></i> <strong>Email</strong>
	            <a href="#">info@pridecreation.com</a>
	          </li>
	        </ul>
	      </div>
	    </div>
	    <!-- contact-info end -->
	    <div class="col-md-6 col-sm-12 col-xs-12">
	      <div class="contact-form">
	        <h3><i class="fa fa-envelope-o"></i> Leave a Message</h3>
	        <div class="row">
	          <form name="frmcontact" id="frmcontact" method="POST">
	            <div class="col-md-6 col-sm-6 col-xs-12">
	              <input name="c_name" id="c_name" type="text" placeholder="Name (required)" required autocomplete="off" autosave="off"/>
	              <span id="errorc_name" class="error-span"></span>
	            </div>
	            <div class="col-md-6 col-sm-6 col-xs-12">
	              <input name="c_email" id="c_email" type="email" placeholder="Email (required)" required autocomplete="off" autosave="off"/>
	              <span id="errorc_email" class="error-span"></span>
	            </div>
	            <div class="col-md-12 col-sm-12 col-xs-12">
	              <input name="c_subject" id="c_subject" type="text" placeholder="Subject" required autocomplete="off" autosave="off"/>
	              <span id="errorc_subject" class="error-span"></span>
	            </div>
	            <div class="col-md-12 col-sm-12 col-xs-12">
	              <textarea name="c_message" id="message" cols="30" rows="10" placeholder="Message"></textarea>
	              <input type="submit" value="Submit Form" />
	            </div>

						
         <center><font color="black"><div id="success_message" style="display:none;">Data Submitted Sucessfully </div></font></center>

	          </form>
	        </div>
	        <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
			<script type="text/javascript" src="js/custom/myjquery.js"></script>
			<script src="js/contact.js"></script>
	      </div>
	    </div>
	  </div>
	</div>
</div>

	<!-- //contact -->
<section id="trial" class="trial text-center wow fadeIn"  data-wow-duration="2s" data-wow-dealy="1.5s">
      <div class="main_trial_area">
          <div class="container">
              <div class="row">
                  <div class="main_trial">
                      <div class="col-md-12">
                          <h2><span>OUR GOAL</span> </h2>
                          <p>Our goal is to give you the best experience and to let your emotions flow on the D-Day so that you can let go all your worries and enjoy the moment.. We focus on natural expressions so when you flip the chapters of your life, you realize what you were feeling or what you did at the same time.</p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>

  <div class="w3_agile_footer">
    <div class="container-fluid" style="background-color: #212121; text-align: center;">
      <p class="text-muted" style="margin-top: 15px;"><font size="3px">© 2017 Pride Creation . All rights reserved | Designed by </font><a href="http://sungare.com">Sungare.</a></p>
      <div class="arrow-container animated fadeInDown">
        <a href="#home" class="arrow-2 scroll scrollToTop">
          <i class="fa fa-angle-up"></i>
        </a>
        <div class="arrow-1 animated hinge infinite zoomIn"></div>
      </div>
    </div>
  </div>
</div>
	<!-- //copyright -->

</body>	
</html>